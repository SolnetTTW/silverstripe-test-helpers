#!/bin/bash

# Solnet SilverStripe test runner - finds other vendor/bin/ss-test-* binaries
# installed by other packages and uses them to run various types of tests.

# Directory we're running in (docroot)
DIR="$(pwd)"

# Directory this script is in (vendor/bin)
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Find what sort of test we want to run
defaulttesttype="all"
TESTTYPE="$defaulttesttype"
if [ ! -z $1 ]; then
    TESTTYPE=$1
fi
echo "Using test type '${TESTTYPE}'"
echo

# Find all sub-package binaries that we can run
alltestbinaries=`ls -1 $SCRIPT_DIR | grep ^ss-test-`

# Output usage instructions
function usage() {
    echo "Usage:"
    echo "    composer test [type] [options]"
    echo
    echo "Types: (default is $defaulttesttype)"
    for testbinary in $alltestbinaries
    do
        # (strips substring from start of var)
        echo "    ${testbinary#ss-test-}"
    done
    echo "    all"
    # Loop thru detected types above
    echo
    echo "Options:"
    echo "    help     Outputs available options for the specified type"
    echo
}

# If we asked for help, print usage
if [ $TESTTYPE = "help" ]; then
    usage
    exit 0
fi

# If we have a requested binary, use it
binary="${SCRIPT_DIR}/ss-test-${TESTTYPE}"
if [ -f $binary ]; then
    echo "Running $binary"
    shift
    $binary "$@"
    exit 0
fi

# If we requested a binary that doesn't exist, print usage
if [ $TESTTYPE != "all" ]; then
    usage
    exit 1
fi

# We didn't request a binary, or requested all binaries
shift
for testtype in $alltestbinaries
do
    binary="${SCRIPT_DIR}/${testtype}"
    $binary "$@"
done
